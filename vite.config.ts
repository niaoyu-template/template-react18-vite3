import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  // base: './',
  // build: {
  //   sourcemap: false, // 默认 false，值为 boolean | 'inline' | 'hidden'
  // },
  server: {
    host: true,
    port: 4002,
  },
  resolve: {
    alias: {
      '@': path.join(__dirname, "src"),
    },
  },
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      }
    },
  },
})
