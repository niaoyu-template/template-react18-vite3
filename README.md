# template-react18-vite3

**vite3 + react18 + redux + antd + less 的 demo 项目，拉取后可直接使用**

### 安装依赖

```shell
npm i
```

### 启动开发环境

```shell
npm run dev
# or
npm start
```

然后再4002端口访问项目，端口配置就在 vite.config.ts 文件中，可以自行修改。

### 打包项目

```shell
npm run build
```

会生成 dist 文件夹，里面就是打包完成后的静态项目。

### 其他

环境配置，在 package.json 的指令 dev 和 build 中，可见有 `--mode dev` 和 `--mode pro` 的字样，这就是在配置环境，但这个配置在项目中无法读取，需要再创建 .env.dev 和 .env.pro 文件，在里面配置对应的变量，在项目中可以访问，详见 [vite 环境变量和模式](https://cn.vitejs.dev/guide/env-and-mode.html)
