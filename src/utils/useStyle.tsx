import { useMemo } from "react";
import { useGlobalState } from '@/globalState'

export default function useStyle(style_pc: any, style_mobile: any) {
  const [isMobile] = useGlobalState('isMobile')
  const style = useMemo(() => {
    return isMobile ? style_mobile : style_pc
   }, [isMobile])
  return style
}