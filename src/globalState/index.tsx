// 这是一个轻量的全局状态管理工具
import { createGlobalState } from 'react-hooks-global-state';
import { mobileWidth } from '@/config'

/**
 * `useGlobalState`: a custom hook works like React.useState
 * `getGlobalState`: a function to get a global state by key outside React
 * `setGlobalState`: a function to set a global state by key outside React
 * `subscribe`: a function that subscribes to state changes
 */
const { useGlobalState, getGlobalState, setGlobalState, subscribe } = createGlobalState({
  // 初始化定义全局变量
  width: document.documentElement.clientWidth,
  isMobile: document.documentElement.clientWidth <= mobileWidth,
  scrollHeight: window.pageYOffset,
  flagNum: 1,
})

export { useGlobalState, getGlobalState, setGlobalState, subscribe }


/* 用法示例

  // 1. useGlobalState 的用法，只能在支持 hook 文件中使用
  import { useGlobalState } from '@/globalState'
  const [flagNum, setflagNum] = useGlobalState('flagNum')

  // 2. 另外三个的用法，可以在不支持的 hook 的文件中使用
  import { getGlobalState, setGlobalState, subscribe } from '@/globalState'
  // 读取
  const flagNum = getGlobalState('flagNum')
  // 修改
  setGlobalState('flagNum', 20)
  // 订阅发生变化
  subscribe('flagNum', value => {
    console.log('flagNum 发生变化：', value)
  })

*/