// import Cookies from 'js-cookie';
import Axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, Canceler, AxiosError, CancelToken, CancelTokenSource } from 'axios';
import { message } from 'antd'

// 扩充传参
interface MyAxiosConfig extends AxiosRequestConfig {
  handle?: boolean, // 是否处理
  tip?: boolean, // 出错是否自动弹出提示(需要配置 handle 值为true)
}

// 返回给接口调用者的数据
type MyAxiosResult = Promise<[boolean, any?, any?]>;

export default class Api {
  private instance: AxiosInstance;
  // 存放取消请求控制器Map
  private abortControllerMap: Map<CancelToken, Canceler>;
  constructor(config: AxiosRequestConfig) {
    this.instance = Axios.create(config)
    this.abortControllerMap = new Map();

    // 全局请求前拦截
    // this.instance.interceptors.request.use(req => req, err => err) 暂不需要，有request方法封装了
    // 全局请求后拦截
    // this.instance.interceptors.response.use(res => res, err => err) 暂不需要，有request方法封装了
  }

  // 要返回的实例
  public async request(config: MyAxiosConfig): MyAxiosResult {
    let serviceResult: AxiosResponse | null = null;
    let serviceError: AxiosError | null = null;

    if (config.handle === undefined) config.handle = true
    if (config.tip === undefined) config.tip = true

    // 请求的中止控制器
    let source: CancelTokenSource | null = null;
    if (!config.cancelToken) {
      source = this.createCancelSource()
      // 添加一个取消请求控制器
      config.cancelToken = source.token
      this.abortControllerMap.set(source.token, source.cancel.bind(source))
    }

    try {
      // 去发起请求
      serviceResult = await this.instance.request(config)
    } catch(error: any) {
      /**
       * 以下情况走此处
       * 1. 服务器返回错误状态码：如果服务器返回错误的状态码，400~599，会抛出一个HTTP错误（HTTP Error）。
       * 2. 其他错误：如果发生其他错误，比如JSON解析错误或请求被中断等，会抛出一个通用错误（General Error）。
       * 3. 网络错误：如果请求失败，比如网络不可用或请求超时，会抛出一个网络错误（Error）。
       * 4. 请求被取消：如果请求被取消，会抛出一个取消错误（Cancel Error）。
       * 注意，1和2时，消息是后台返回的，可能会有错误信息
       */
      if (error.response) {
        serviceResult = error.response as AxiosResponse
      } else {
        console.error(error)
        serviceError = error as AxiosError
      }
    }

    // 请求已完成，删除这个请求的取消请求控制器
    if (source) this.abortControllerMap.delete(source.token);

    if (serviceResult) {
      return this.dataHandler(serviceResult, config)
    } else if (serviceError) {
      return this.errorHandler(serviceError, config)
    }

    return [false, false, null];
  }

  /**
   * 创建一个取消请求的source
   * 设置某个请求的配置 config.cancelToken = source.token
   * 再调用 source.cancel() 即可取消这个请求
   * @returns {CancelTokenSource}
   */
  public createCancelSource(): CancelTokenSource {
    return Axios.CancelToken.source()
  }

  // 返回结果的处理程序
  private async dataHandler(response: AxiosResponse, config: MyAxiosConfig): MyAxiosResult {
    let success: boolean = true;
    let data: any = null;

    // 需要处理
    if (config.handle) {
      data = response.data
      let tipMsg = ''

      // 需要提示切有错误提示
      if (config.tip && tipMsg) { // 需要弹框提醒
        message.error(tipMsg)
      }
    }

    return [success, data, response]
  }

  // 服务出错/取消请求的处理
  private async errorHandler(error: AxiosError, config: MyAxiosConfig): MyAxiosResult {
    let success: boolean = true;
    let data: any = null;
    if (config.handle) {
      success = false
      // 需要弹框
      if (config.tip) {
        // 超时
        let tipMsg = ''
        if (error?.message?.includes('timeout')) {
          tipMsg = 'Server connection failure!' // 请求超时
        } else if (error?.message?.includes('Network Error')) {
          tipMsg = 'Network connection failure!' // 网络错误
        } else {
          tipMsg = 'Service error, please try again later!'
        }
        message.error(tipMsg)
      }
    }

    return [success, data, error]
  }
}
