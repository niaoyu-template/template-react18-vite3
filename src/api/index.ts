import Api from './Api'

// 创建一个 axios 服务
const service = new Api({
  // 默认配置们
  baseURL: import.meta.env.VITE_API_URL,
  timeout: 1000 * 60, // 超时时间, 毫秒
  // withCredentials: true, // 发送请求时，是否带 cookie
  headers: {
    'Content-Type': 'application/json; charset=utf-8'
  }
})

export default service.request.bind(service);

// 导出「创建一个取消请求的source」的方法
export const createCancelSource = service.createCancelSource.bind(service);
