import ReactDOM from "react-dom/client";
import Router from "./router";
import './assets/css/index.less';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLDivElement);
root.render(
  <Router />
)