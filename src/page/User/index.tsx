import React from "react";
import { Button } from 'antd'
import useStyle from "@/utils/useStyle";
import style_pc from "./pc.module.less";
import style_mobile from "./mobile.module.less";
import { useGlobalState } from "@/globalState";

export default function User() {
  const style = useStyle(style_pc, style_mobile)

  const [flagNum, setflagNum] = useGlobalState('flagNum');
  const addFlag = () => {
    setflagNum(old => old + 1)
  }
  return (
    <div className={style.User}>
      <p>user页</p>
      <Button onClick={addFlag}>点击加值：{flagNum}</Button>
    </div>
  );
}