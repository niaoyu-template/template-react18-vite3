import React from "react";
// import { Link, useLocation } from "react-router-dom";
import { useGlobalState } from "@/globalState";
import { Button } from 'antd'
// import { PieChartOutlined, DesktopOutlined } from "@ant-design/icons";
import useStyle from "@/utils/useStyle";
import style_pc from "./pc.module.less";
import style_mobile from "./mobile.module.less";

export default function Home() {
  const style = useStyle(style_pc, style_mobile)

  // 获取全局变量
  const [flagNum, setflagNum] = useGlobalState('flagNum');
  const addFlag = () => {
    setflagNum(old => old + 1)
  }
  return (
    <div className={style.Home}>
      <p>home页</p>
      <span style={{backgroundColor: 'var(--main)'}}>正常颜色</span>
      <span style={{backgroundColor: 'var(--multiply_color)'}}>multiply 后的颜色混合</span>
      <span style={{backgroundColor: 'var(--screen_color)'}}>screen 后的颜色混合</span>
      <Button onClick={addFlag}>点击加值：{flagNum}</Button>
    </div>
  );
}