import { Spin } from 'antd';

export default function Loading() {
  const style = {
    width: '100vw',
    height: '100vh',
    margin: '0 auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }
  return <div style={style}><Spin /></div>
}