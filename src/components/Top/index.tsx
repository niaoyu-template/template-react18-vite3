import { useState, useEffect } from "react";
import useStyle from "@/utils/useStyle";
import style_pc from "./pc.module.less";
import style_mobile from "./mobile.module.less";
import { Link, useLocation } from "react-router-dom";
import { useGlobalState } from "@/globalState";
import { Menu, Button } from 'antd'
import { PieChartOutlined, DesktopOutlined } from "@ant-design/icons";

export default function Top() {
  const style = useStyle(style_pc, style_mobile)
  const location = useLocation();
  const [selectedKeys, setSelectedKeys] = useState([location.pathname])

  useEffect(() => {
    setSelectedKeys([location.pathname])
  }, [location])

  const [flagNum, setflagNum] = useGlobalState('flagNum');
  const addFlag = () => {
    setflagNum(old => old + 1)
  }

  return (
    <div className={style.Top}>
      <div>
        <div className={style.main}>
          <Menu
            defaultSelectedKeys={selectedKeys}
            mode="horizontal"
            items={[
              {
                label: <Link to="/">首页</Link>,
                icon: <PieChartOutlined />,
                key: '/',
              },
              {
                label: <Link to="/user">用户页</Link>,
                icon: <DesktopOutlined />,
                key: '/user',
              },
            ]}
          />
          <div className="flex">
            <span style={{paddingRight: 10}}>当前环境：{import.meta.env.VITE_ENV}</span>
            <Button onClick={addFlag}>当前FlagNum：{flagNum}</Button>
          </div>
        </div>
      </div>
    </div>
  );
}
