import { useEffect, useCallback } from 'react'
import { useGlobalState } from '@/globalState'
import { mobileWidth } from '@/config'

export default function useInit() {
  // 页面宽度监听
  const [width, setwidth] = useGlobalState('width')
  const setWidth = useCallback(() => {
    setwidth(document.documentElement.clientWidth)
  }, [])
  useEffect(() => {
    window.addEventListener('resize', setWidth)
    return () => {
      window.removeEventListener('resize', setWidth)
    }
  }, [])

  // 判断是否为移动端
  const [isMobile, setisMobile] = useGlobalState('isMobile')
  useEffect(() => {
    const newIsMobile = width <= mobileWidth
    if (newIsMobile !== isMobile) setisMobile(newIsMobile)
  }, [width])
  useEffect(() => {
    const classList = (document.querySelector('html') as Element).classList
    if (isMobile) {
      classList.remove('isPc')
      classList.add('isMobile')
    } else {
      classList.remove('isMobile')
      classList.add('isPc')
    }
  }, [isMobile])

  // 滚动监听
  const [, setscrollHeight] = useGlobalState('scrollHeight')
  const onScroll = useCallback(() => {
    const pageYOffset = window.pageYOffset
    setscrollHeight(pageYOffset)
  }, [])
  useEffect(() => {
    window.addEventListener('scroll', onScroll)
    return () => {
      window.removeEventListener('scroll', onScroll)
    }
  }, [onScroll])
}