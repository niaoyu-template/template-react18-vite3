// 这里引入 lazy、Suspense 两个依赖
import { lazy, Suspense } from "react";
import { /* BrowserRouter, */ HashRouter, Route, Routes, Navigate } from "react-router-dom";
import classnames from 'classnames'
import Top from '@/components/Top'
import Loading from '@/components/Loading'
import useInit from "./useInit"
// import { useGlobalState } from '@/globalState'
// 引入页面组件的写法修改
const Home = lazy(() => import("@/page/Home"))
const User = lazy(() => import("@/page/User"))

export default function Router() {
  useInit()

  return (
    <div className={classnames('App')}>
      {/* 使用 Suspense 组件包裹，使用懒加载引入的组件路由，必须是这个组件的子组件 */}
      {/* 但这里面也可以写正常引入的组件 */}
      {/* fallback 是当正在加载页面时的展示内容，如果不加就是白屏 */}
      <HashRouter>
        <Top />
        <Suspense fallback={<Loading />}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/user" element={<User />} />
            <Route path="/*" element={<Navigate to="/" />}></Route>
          </Routes>
        </Suspense>
      </HashRouter>
    </div>
  );
}